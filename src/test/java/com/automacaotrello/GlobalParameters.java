package com.automacaotrello;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GlobalParameters {

	public static String URL;
	public static String TOKEN;
	public static String KEY;
	public static String ID_LIST;
	
	public static String REPORT_NAME;
	public static String REPORT_PATH;
	
	private Properties properties;

	public GlobalParameters() {

		properties = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("globalParameters.properties");
			properties.load(input);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		URL = properties.getProperty("dev.url");
		TOKEN = properties.getProperty("dev.token");
		KEY = properties.getProperty("dev.key");
		ID_LIST = properties.getProperty("dev.idList");
		
		REPORT_NAME = properties.getProperty("report.name");
		REPORT_PATH = properties.getProperty("report.path");

	}
}
