package com.automacaotrello.tests.card;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.automacaotrello.bases.TestBase;
import com.automacaotrello.steps.card.CriarCardSteps;
import com.automacaotrello.steps.card.DeleteCardSteps;
import com.automacaotrello.steps.card.EditarCardSteps;

public class CardTests extends TestBase {

	@Test
	public void quandoCriarUmCardDeveEditarEExcluirCardComSucesso() {

		SoftAssert softAssert = new SoftAssert();

		int statusCodeEsperado = HttpStatus.SC_OK;
		String idCard;
		int criarStatusCode;
		int editarStatusCode;
		int deleteStatusCode;
		
		CriarCardSteps.criarCard();
		idCard = CriarCardSteps.getIdCard();
		criarStatusCode = CriarCardSteps.getStatusCode();
		EditarCardSteps.editarCard(idCard);
		editarStatusCode = EditarCardSteps.getStatusCode();
		DeleteCardSteps.deleteCard(idCard);
		deleteStatusCode = DeleteCardSteps.getStatusCode();

		softAssert.assertEquals(criarStatusCode, statusCodeEsperado);
		softAssert.assertEquals(editarStatusCode, statusCodeEsperado);
		softAssert.assertEquals(deleteStatusCode, statusCodeEsperado);
		softAssert.assertAll();

	}

	@Test
	public void quandoCraiarUmCardDeveValidarContrato() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEsperado = HttpStatus.SC_OK;
		String idCard;
		int criarStatusCode;
		int deleteStatusCode;
		
		CriarCardSteps.criarCard();
		idCard = CriarCardSteps.getIdCard();
		criarStatusCode = CriarCardSteps.getStatusCode();
		DeleteCardSteps.deleteCard(idCard);
		deleteStatusCode = DeleteCardSteps.getStatusCode();
		
		CriarCardSteps.validarContrato();
		softAssert.assertEquals(criarStatusCode, statusCodeEsperado);
		softAssert.assertEquals(deleteStatusCode, statusCodeEsperado);
		softAssert.assertAll();
	}

	@Test
	public void CriandoCardTrelloComListaInvalida() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;
		String mensagemEsperada = "invalid value for idList";
		String idListTest = "606721999999202d538216a";
		String mensagem;
		int criarStatusCode;
		
		CriarCardSteps.criarCard(idListTest);
		criarStatusCode = CriarCardSteps.getStatusCode();
		mensagem = CriarCardSteps.getMensagem();

		softAssert.assertEquals(criarStatusCode, statusCodeEsperado);
		softAssert.assertEquals(mensagem, mensagemEsperada);
		softAssert.assertAll();

	}

	@Test
	public void criarCardTrelloComListaQueNaoExisteNoQuadro() {

		SoftAssert softAssert = new SoftAssert();

		int statusCodeEsperado = HttpStatus.SC_NOT_FOUND;
		String mensagemEsperada = "could not find the board that the card belongs to";
		String idListTest = "706721936cf19202d538216a";
		String mensagem;
		int criarStatusCode;
		
		CriarCardSteps.criarCard(idListTest);
		criarStatusCode = CriarCardSteps.getStatusCode();
		mensagem = CriarCardSteps.getMensagem();

		softAssert.assertEquals(criarStatusCode, statusCodeEsperado);
		softAssert.assertEquals(mensagem, mensagemEsperada);
		softAssert.assertAll();

	}

	@Test
	public void editarCardPassandoIdInvalidosDoCard() {
		
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;
		String mensagemEsperada = "invalid id";
		String idCard = "99999999999999999";
		int editarStatusCode;
		String mensagem;
		
		EditarCardSteps.editarCard(idCard);
		editarStatusCode = EditarCardSteps.getStatusCode();
		mensagem = EditarCardSteps.getMensagem();

		softAssert.assertEquals(editarStatusCode, statusCodeEsperado);
		softAssert.assertEquals(mensagem, mensagemEsperada);
		softAssert.assertAll();

	}

	@Test
	public void editarCardJaExcluido() {
		
		SoftAssert softAssert = new SoftAssert();
		int statusCodeEsperadoOk = HttpStatus.SC_OK;
		int statusCodeEsperadoNoFound = HttpStatus.SC_NOT_FOUND;
		String mensagemEsperada = "The requested resource was not found.";
		String idCard;
		int criarStatusCode;
		int editarStatusCode;
		int deleteStatusCode;
		String mensagem;
		
		CriarCardSteps.criarCard();
		idCard = CriarCardSteps.getIdCard();
		criarStatusCode = CriarCardSteps.getStatusCode();
		DeleteCardSteps.deleteCard(idCard);
		deleteStatusCode = DeleteCardSteps.getStatusCode();
		EditarCardSteps.editarCard(idCard);
		editarStatusCode = EditarCardSteps.getStatusCode();
		mensagem = EditarCardSteps.getMensagem();

		softAssert.assertEquals(criarStatusCode, statusCodeEsperadoOk);
		softAssert.assertEquals(deleteStatusCode, statusCodeEsperadoOk);
		softAssert.assertEquals(editarStatusCode, statusCodeEsperadoNoFound);
		softAssert.assertEquals(mensagem, mensagemEsperada);
		softAssert.assertAll();
	}

	@Test
	public void excluirCardJaExcluido() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEsperadoOk = HttpStatus.SC_OK;
		int statusCodeEsperadoNoFound = HttpStatus.SC_NOT_FOUND;
		String mensagemEsperada = "The requested resource was not found.";
		String idCard;
		String mensagem;
		int criarStatusCode;
		int deleteSucessoStatusCode;
		int deleteFailStatusCode;

		CriarCardSteps.criarCard();
		idCard = CriarCardSteps.getIdCard();
		criarStatusCode = CriarCardSteps.getStatusCode();
		DeleteCardSteps.deleteCard(idCard);
		deleteSucessoStatusCode = DeleteCardSteps.getStatusCode();
		
		DeleteCardSteps.deleteCard(idCard);
		deleteFailStatusCode = DeleteCardSteps.getStatusCode();
		mensagem = DeleteCardSteps.getMensagem();

		softAssert.assertEquals(criarStatusCode, statusCodeEsperadoOk);
		softAssert.assertEquals(deleteSucessoStatusCode, statusCodeEsperadoOk);
		softAssert.assertEquals(deleteFailStatusCode, statusCodeEsperadoNoFound);
		softAssert.assertEquals(mensagem, mensagemEsperada);
		softAssert.assertAll();
	}

}
