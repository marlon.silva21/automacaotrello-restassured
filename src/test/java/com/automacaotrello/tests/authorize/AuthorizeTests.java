package com.automacaotrello.tests.authorize;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.automacaotrello.bases.TestBase;
import com.automacaotrello.requests.steps.authorize.AuthorizeSteps;

public class AuthorizeTests extends TestBase{
	
	@Test
	public void dadoKeyETokenValidosQuandoRequestUserDeveMostrarDadosDoUsuario() {

		SoftAssert softAssert = new SoftAssert();
		
		int statusCodeEsperado = HttpStatus.SC_OK;
		int statusCodeSucesso;
		
		AuthorizeSteps.confirmarAutorizacao();
		statusCodeSucesso = AuthorizeSteps.getStatusCode();

		softAssert.assertEquals(statusCodeSucesso, statusCodeEsperado);
		softAssert.assertAll();

	}
	
	@Test
	public void dadoKeyETokenInvalidosQuandoRequestUserNaoDeveMostrarDadosDoUsuario() {

		SoftAssert softAssert = new SoftAssert();
		
		String mensagemEsperada = "invalid token";
		String mensagem;
		String invalidKey = "d9c87552a483679f968f49783cd6e4ed";
		String invalidToken = "2a22e766f8dd63059081f1c59a823526cb4b6ca053699cf567f1436acf5ac806";
		int statusCodeEsperado = HttpStatus.SC_UNAUTHORIZED;
		int statusCodeSucesso;
		
		AuthorizeSteps.confirmarAutorizacao(invalidKey, invalidToken);
		statusCodeSucesso = AuthorizeSteps.getStatusCode();
		mensagem = AuthorizeSteps.getMensagem();
		

		softAssert.assertEquals(statusCodeSucesso, statusCodeEsperado);
		softAssert.assertEquals(mensagem, mensagemEsperada);
		softAssert.assertAll();

	}
	
	@Test
	public void dadoKeyETokenValidosQuandoRequestUserValidarContrato() {

		SoftAssert softAssert = new SoftAssert();
		
		int statusCodeEsperado = HttpStatus.SC_OK;
		int statusCodeSucesso;
		
		AuthorizeSteps.confirmarAutorizacao();
		statusCodeSucesso = AuthorizeSteps.getStatusCode();
		
		AuthorizeSteps.validarContrato();
		softAssert.assertEquals(statusCodeSucesso, statusCodeEsperado);
		softAssert.assertAll();

	}
}
