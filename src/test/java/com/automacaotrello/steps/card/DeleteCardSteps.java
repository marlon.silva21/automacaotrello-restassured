package com.automacaotrello.steps.card;

import com.automacaotrello.requests.card.DeleteCardRequest;

import io.restassured.response.Response;

public class DeleteCardSteps {
	
	private static Response deleteCardResponse;

	public static void deleteCard(String idCard) {
		DeleteCardRequest deleteCardRequest = new DeleteCardRequest(idCard);
		deleteCardResponse = deleteCardRequest.executeRequest();
	}
	
	public static int getStatusCode() {

		return deleteCardResponse.getStatusCode();
	}

	public static String getMensagem() {

		return deleteCardResponse.body().asString();
	}
}
