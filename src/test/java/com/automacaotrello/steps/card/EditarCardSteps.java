package com.automacaotrello.steps.card;

import com.automacaotrello.requests.card.PutCardRequest;

import io.restassured.response.Response;

public class EditarCardSteps {
	
	private static Response editarCardResponse;

	public static void editarCard(String idCard) {
		PutCardRequest putCardRequest = new PutCardRequest(idCard);
		editarCardResponse = putCardRequest.executeRequest();
	}

	public static int getStatusCode() {

		return editarCardResponse.getStatusCode();
	}
	
	public static String getMensagem() {

		return editarCardResponse.body().asString();
	}

}
