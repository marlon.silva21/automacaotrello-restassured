package com.automacaotrello.steps.card;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import com.automacaotrello.requests.card.PostCardRequest;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class CriarCardSteps {

	private static Response criarCardResponse;

	public static void criarCard() {
		PostCardRequest postCardRequest = new PostCardRequest();
		criarCardResponse = postCardRequest.executeRequest();
	}
	
	public static void criarCard(String idListTest) {
		PostCardRequest postCardRequest = new PostCardRequest(idListTest);
		criarCardResponse = postCardRequest.executeRequest();
	}

	public static String getIdCard() {

		return criarCardResponse.body().jsonPath().get("id").toString();
	}
	
	public static String getMensagem() {

		return criarCardResponse.body().asString();
	}

	public static int getStatusCode() {

		return criarCardResponse.getStatusCode();
	}

	public static ValidatableResponse validarContrato() {

		return criarCardResponse.then().assertThat().body(matchesJsonSchemaInClasspath("contract/PostCardContract.json"));
	}
}
