package com.automacaotrello.bases;

import java.util.HashMap;
import java.util.Map;

import com.automacaotrello.GlobalParameters;
import com.automacaotrello.utils.ExtentReportsUtils;
import com.automacaotrello.utils.RestAssuredUtils;

import io.restassured.http.Method;
import io.restassured.response.Response;

public class RequestRestBase {

	protected String url = GlobalParameters.URL;
	protected String requestService = null;
	protected Method method = null;
	protected Object form = null;
	protected Map<String, String> headers = new HashMap<String, String>();
	protected Map<String, String> queryParameters = new HashMap<String, String>();
	protected String token = GlobalParameters.TOKEN;
	protected String key = GlobalParameters.KEY;
	protected String idList = GlobalParameters.ID_LIST;
	protected Response response;

	public RequestRestBase() {
		headers.put("content-type", "application/json");
	}

	public Response executeRequest() {
		Response response = RestAssuredUtils.executeRestRequest(url, requestService, method, headers, queryParameters,
				form);

		ExtentReportsUtils.addRestTestInfo(url, requestService, method.toString(), headers, response);

		return response;
	}
}
