package com.automacaotrello.bases;

import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.automacaotrello.GlobalParameters;
import com.automacaotrello.utils.ExtentReportsUtils;

public class TestBase {

	@BeforeSuite
	public void beforSuite() {
		new GlobalParameters();
		ExtentReportsUtils.createReport();
	}

	@BeforeMethod
	public void beforeTest(Method method) {
		ExtentReportsUtils.addTest(method.getName(), method.getDeclaringClass().getSimpleName());
	}

	@AfterMethod
	public void afterTest(ITestResult result) {
		ExtentReportsUtils.addTestResult(result);
	}

	@AfterSuite
	public void afterSuite() {
		ExtentReportsUtils.generateReport();
	}
}
