package com.automacaotrello.utils;
import java.net.URI;
import java.util.Map;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredUtils {
	

	public static Response executeRestRequest(String url, String requestService, Method method,
			Map<String, String> headers, Map<String, String> queryParameters, Object form) {

		RequestSpecification requestSpecification = RestAssured.given();

		for (Map.Entry<String, String> header : headers.entrySet()) {
			requestSpecification.headers(header.getKey(), header.getValue());
		}

		for (Map.Entry<String, String> parameter : queryParameters.entrySet()) {
			requestSpecification.queryParams(parameter.getKey(), parameter.getValue());
		}

		if (form != null) {
			requestSpecification.body(form);
		}

		return requestSpecification.request(method, URI.create(url + requestService));
	}

}
