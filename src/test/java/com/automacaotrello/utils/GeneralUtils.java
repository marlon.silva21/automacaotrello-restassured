package com.automacaotrello.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.ITestResult;

public class GeneralUtils {
	
	public static String getNowDate(String mask) {
		DateFormat dateFormat = new SimpleDateFormat(mask);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String getMethodNameByLevel(int level) {
		StackTraceElement[] stackTrace = new Throwable().getStackTrace();

		return stackTrace[level].getMethodName();
	}
	
	public static String formatJson(final String json) {
		final int indent_width = 4;
		final char[] chars = json.toCharArray();
		final String newline = System.lineSeparator();

		String ret = "";
		boolean begin_quotes = false;

		for (int i = 0, indent = 0; i < chars.length; i++) {
			char c = chars[i];

			if (c == '\"') {
				ret += c;
				begin_quotes = !begin_quotes;
				continue;
			}

			if (!begin_quotes) {
				switch (c) {
				case '{':
				case '[':
					ret += c + newline + String.format("%" + (indent += indent_width) + "s", "");
					continue;
				case '}':
				case ']':
					ret += newline + ((indent -= indent_width) > 0 ? String.format("%" + indent + "s", "") : "") + c;
					continue;
				case ':':
					ret += c + " ";
					continue;
				case ',':
					ret += c + newline + (indent > 0 ? String.format("%" + indent + "s", "") : "");
					continue;
				default:
					if (Character.isWhitespace(c))
						continue;
				}
			}

			ret += c + (c == '\\' ? "" + chars[++i] : "");
		}

		return ret;
	}
	
	public static String getAllStackTrace(ITestResult result) {
		String allStackTrace = "";

		for (StackTraceElement stackTrace : result.getThrowable().getStackTrace()) {
			allStackTrace = allStackTrace + "<br>" + stackTrace.toString();
		}

		return allStackTrace;
	}

}
