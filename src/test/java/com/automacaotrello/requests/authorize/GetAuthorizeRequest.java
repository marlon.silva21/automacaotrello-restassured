package com.automacaotrello.requests.authorize;

import com.automacaotrello.bases.RequestRestBase;

import io.restassured.http.Method;

public class GetAuthorizeRequest extends RequestRestBase {

	public GetAuthorizeRequest() {
		requestService = "/members/me";
		method = Method.GET;
		queryParameters.put("key", key);
		queryParameters.put("token", token);
	}
	
	public GetAuthorizeRequest(String auxKey, String auxToken) {
		requestService = "/members/me";
		method = Method.GET;
		queryParameters.put("key", auxKey);
		queryParameters.put("token", auxToken);
	}
	
	
}
