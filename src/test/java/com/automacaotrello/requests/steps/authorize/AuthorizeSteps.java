package com.automacaotrello.requests.steps.authorize;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import com.automacaotrello.requests.authorize.GetAuthorizeRequest;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class AuthorizeSteps {

	private static Response authorizeResponse;

	public static void confirmarAutorizacao() {
		GetAuthorizeRequest getAuthorizeRequest = new GetAuthorizeRequest();
		authorizeResponse = getAuthorizeRequest.executeRequest();
	}
	
	public static void confirmarAutorizacao(String auxKey, String auxToken) {
		GetAuthorizeRequest getAuthorizeRequest = new GetAuthorizeRequest(auxKey, auxToken);
		authorizeResponse = getAuthorizeRequest.executeRequest();
	}
	
	public static String getIdCard() {

		return authorizeResponse.body().jsonPath().get("id").toString();
	}
	
	public static String getMensagem() {

		return authorizeResponse.body().asString();
	}

	public static int getStatusCode() {

		return authorizeResponse.getStatusCode();
	}
	
	public static ValidatableResponse validarContrato() {

		return authorizeResponse.then().assertThat().body(matchesJsonSchemaInClasspath("contract/AuthorizeContract.json"));
	}

}
