package com.automacaotrello.requests.card;

import com.automacaotrello.bases.RequestRestBase;

import io.restassured.http.Method;

public class PostCardRequest extends RequestRestBase {

	public PostCardRequest() {

		requestService = "/card";
		method = Method.POST;
		queryParameters.put("key", key);
		queryParameters.put("token", token);
		queryParameters.put("idList", idList);
		queryParameters.put("name", "Card Test");
	}
	
	public PostCardRequest(String idListTest) {

		requestService = "/card";
		method = Method.POST;
		queryParameters.put("key", key);
		queryParameters.put("token", token);
		queryParameters.put("idList", idListTest);
		queryParameters.put("name", "Card Test");
	}

}
