package com.automacaotrello.requests.card;

import com.automacaotrello.bases.RequestRestBase;

import io.restassured.http.Method;

public class PutCardRequest extends RequestRestBase {

	public PutCardRequest(String idCard) {

		requestService = "/cards/" + idCard;
		method = Method.PUT;
		queryParameters.put("key", key);
		queryParameters.put("token", token);
		queryParameters.put("name", "Name Card Edit");
	}

}
