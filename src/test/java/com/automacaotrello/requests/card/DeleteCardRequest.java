package com.automacaotrello.requests.card;

import com.automacaotrello.bases.RequestRestBase;

import io.restassured.http.Method;

public class DeleteCardRequest extends RequestRestBase{
	
	public DeleteCardRequest(String idCard) {
		requestService = "/cards/"+idCard;
		method = Method.DELETE;
		queryParameters.put("key", key);
		queryParameters.put("token", token);
	}

}
