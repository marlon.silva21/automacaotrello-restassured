## 🚀 Restassured test automation api trello🚀

Projeto para estudo e treino de arquitetura.

### ⚙️ Arquitetura do projeto
-----------------------

```
automacaotrello-restassured/
  ├─  src/test/java/
  │        │
  │        ├── bases/
  │        │
  │        ├── requests/
  │        │
  │        ├── steps/
  │        │
  │        ├── tests/
  │        │
  │        ├── utils/
  │ 
  ├── Dockerfile
  ├── globalParameters.properties
  ├── TestNg.xml
  ├── pom.xml
  └── README.md
```
## 🔍 Camadas da arquitetura
- **bases:** 
- **requests:** 
- **steps:** 
- **tests:** 
- **utils:** 
- **globalParameters.properties:** 
- **TestNg.xml:** 
- **Dockerfile:** 

## Executando automação - docker
```
docker run -i --name autotrello marlonpestana/automacao mvn clean test
```

